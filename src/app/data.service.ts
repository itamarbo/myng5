import { Injectable } from '@angular/core';
import { Observable, observable } from 'rxjs'; 
import { HttpClientModule, HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  myUrl='https://jsonplaceholder.typicode.com/users';

  constructor(public httpClient:HttpClient) { }

  getUsers(){
    return this.httpClient.get(this.myUrl);
  }

  addUser(user){
    return this.httpClient.post(this.myUrl, user);
  }

  deleteUser(id){
    return this.httpClient.delete(this.myUrl +'/'+ id);
  }

  updateUser(user){
    return this.httpClient.put(this.myUrl +'/'+ user.id,user);
  }
   
}
