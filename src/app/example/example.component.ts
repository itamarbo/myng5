import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.css']
})
export class ExampleComponent  {

  users:any=[];
  user = {
    id:'',
    name:'',
    email:'',
    phone:''
  }

  isEdit:boolean = false;

  constructor(public dataService:DataService) {
    
    this.dataService.getUsers().subscribe(users =>{
      console.log(users);
      this.users = users;
    })
   }

   onSend(isEdit){
    if (isEdit) {
      this.dataService.updateUser(this.user).subscribe(user =>{
        console.log(user);
        for(let i=0; i < this.users.length; i++){
          if(this.users[i].id == this.user.id){
             this.users.splice(i,1);
          }
        }
        this.users.unshift(this.user);
        this.isEdit=false;
      });
      
    }
    else{
    this.dataService.addUser(this.user).subscribe(user =>{
    this.users.unshift(user);
    //   this.user.name = ''; // לאחר הוספת המשתמש, זה בעצם מנקה לנו את הטופס לאחר מכן
    //   this.user.email = ''; // לאחר הוספת המשתמש, זה בעצם מנקה לנו את הטופס לאחר מכן
    //   this.user.phone = ''; // לאחר הוספת המשתמש, זה בעצם מנקה לנו את הטופס לאחר מכן

     });
   }}

   onDelete(id){
    this.dataService.deleteUser(id).subscribe(res =>{
      console.log(res);
      for(let i=0; i < this.users.length; i++){
        if(this.users[i].id == id){
          this.users.splice(i,1);
        }
      }
    });
  }

  onEdit(user){
    this.isEdit = true;
    this.user=user;
    console.log(user)
  }

}
