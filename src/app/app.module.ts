import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { NavComponent } from './nav/nav.component';
import { FormComponent } from './form/form.component';

import { FormsModule }   from '@angular/forms';
import { ExampleComponent } from './example/example.component';
import { DataService } from './data.service';
import { HttpClientModule } from '@angular/common/http';
import { TodoComponent } from './todo/todo.component';



const appRoutes: Routes = [
  { path: 'about', component: AboutComponent },
  { path: 'form', component: FormComponent },
  { path: 'example', component: ExampleComponent },
  { path: 'todo', component: TodoComponent },
  { path: '', component: HomeComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    NavComponent,
    FormComponent,
    ExampleComponent,
    TodoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
