import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  name:string='';
  hobbies:string[]=['I love to code in angular'];

  constructor() { }

  ngOnInit() {
    console.log(this.hobbies.indexOf)
  }

  onSubmit() {
    this.hobbies.push(this.name);
    this.name='';
  }

  onDelete(i){
    this.hobbies.splice(i,1)
  }

}
