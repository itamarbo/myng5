import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'myng5';
  val = 300;

  someVal(){
    this.val=this.val+300;
  }

  constructor() { this.someVal();}
}
